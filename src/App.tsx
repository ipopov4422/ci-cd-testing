import React from 'react'
import logo from './logo.svg'
import './App.css'

function App() {
  return (
    <div className='App'>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className='App-link'
          href='https://reactjs.org'
          target='_blank'
          rel='noopener noreferrer'
        >
          Learn React
        </a>
      </header>
      <div className='define-box'>
        <div className='price-table light'>
          <div className='price-table-title edit'>
            Estimate breakdown
            <span className='edit'>
              <svg
                width='24'
                height='24'
                viewBox='0 0 24 24'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
              >
                <path
                  d='M10 12C10 13.1046 10.8954 14 12 14C13.1046 14 14 13.1046 14 12C14 10.8954 13.1046 10 12 10C10.8954 10 10 10.8954 10 12Z'
                  fill='#9A98BF'
                ></path>
                <path
                  d='M17 12C17 13.1046 17.8954 14 19 14C20.1046 14 21 13.1046 21 12C21 10.8954 20.1046 10 19 10C17.8954 10 17 10.8954 17 12Z'
                  fill='#9A98BF'
                ></path>
                <path
                  d='M3 12C3 13.1046 3.89543 14 5 14C6.10457 14 7 13.1046 7 12C7 10.8954 6.10457 10 5 10C3.89543 10 3 10.8954 3 12Z'
                  fill='#9A98BF'
                ></path>
              </svg>
            </span>
          </div>
          <span className='price-table-info'>
            Estimated breakdown uses default values to provide an estimate.
            These default values will be updated with accurate values once
            reviewed by our team.
          </span>
          <div className='price-table-body'>
            <div className='price-table-row'>
              Wanted completes<span>3000</span>
            </div>
            <div className='price-table-row'>
              Feasible completes<span>150</span>
            </div>
            <div className='price-table-row'>
              CPI<span>2.39 GBP</span>
            </div>
          </div>
          <div className='price-table-footer'>
            <div className='price-table-row'>
              Subtotal<span>7170.00 GBP</span>
            </div>
            <div className='price-table-row'>
              VAT<span>1434.00 GBP</span>
            </div>
          </div>
          <div className='price-table-total'>
            <div className='price-table-row'>
              <div className='tooltip-wrapper'>
                <div className='tooltip up light'>
                  <span className='tooltip-head'>
                    <svg
                      width='14'
                      height='14'
                      fill='none'
                      xmlns='http://www.w3.org/2000/svg'
                    >
                      <path
                        d='M7 0a7 7 0 100 14A7 7 0 007 0zm0 12.833A5.833 5.833 0 117 1.167a5.833 5.833 0 010 11.666z'
                        fill='#400A67'
                      ></path>
                      <path
                        d='M7 5.658a.583.583 0 00-.583.584v3.966a.583.583 0 101.167 0V6.242A.583.583 0 007 5.658zm0-2.391a.618.618 0 00-.595.606v.094A.548.548 0 007 4.515a.607.607 0 00.584-.583v-.14A.525.525 0 007 3.267z'
                        fill='#400A67'
                      ></path>
                    </svg>
                  </span>
                  <p className='tooltip-content'>
                    The actual cost of your survey is determined once your
                    campaign has gone live but we can estimate the cost of a
                    campaign through assessing a mixture of factors including
                    the incidence rate, length of interview, target location and
                    the number of completes you have requested. These factors
                    will be determined through the course of the Define phase.
                  </p>
                </div>
              </div>
              <span className='medium'>Total</span>
              <span className='big'>8604.00 GBP</span>
            </div>
          </div>
          <div className='price-table-action'>
            <div className='price-table-action-button special'>
              <button type='button' className='button icon-left'>
                <svg
                  width='12'
                  height='12'
                  viewBox='0 0 12 12'
                  fill='none'
                  xmlns='http://www.w3.org/2000/svg'
                >
                  <path
                    d='M6 1V11'
                    stroke='#DF0E6E'
                    stroke-width='2'
                    stroke-linecap='round'
                    stroke-linejoin='round'
                  ></path>
                  <path
                    d='M1 6H11'
                    stroke='#DF0E6E'
                    stroke-width='2'
                    stroke-linecap='round'
                    stroke-linejoin='round'
                  ></path>
                </svg>
                Submit Project
              </button>
              <div className='price-table-action-button-tooltip'>
                Once you have completed your survey brief please click here to
                submit it for review
              </div>
            </div>
            <div className='price-table-action-button'>
              <button type='button' className='button'>
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default App
