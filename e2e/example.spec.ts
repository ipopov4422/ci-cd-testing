import { test, expect } from '@playwright/test';

test('homepage has Playwright in title and get started link linking to the intro page', async ({ page }) => {
  // await page.goto('https://ipopov4422.gitlab.io/ci-cd-testing/');
  await page.goto('./');

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/React App/);

  // create a locator
  const getStarted = page.locator('text=Learn react');

  // Expect an attribute "to be strictly equal" to the value.
  await expect(getStarted).toHaveAttribute('href', 'https://reactjs.org');
});
