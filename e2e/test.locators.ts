import { test, expect } from '@playwright/test'

test('Testing locators', async ({ page }) => {
  await page.goto('./')

  // find nested element with text
  expect(
    await page.locator('text="Wanted completes"', {
      has: page.locator('text=3000')
    })
  ).toBeVisible()

  // Find element on left or right side from another element
  expect(
    await page.locator(':text("8604.00 GBP"):right-of(:text("Total"))')
  ).toBeVisible()
})
